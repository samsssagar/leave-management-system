package com.productmanagement.learningmanage.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.productmanagement.learningmanage.model.CustomUserDetails;
import com.productmanagement.learningmanage.model.User;
import com.productmanagement.learningmanage.repository.UserRepo;

@Service
public class CustomUserDetailsService implements UserDetailsService{
	
	@Autowired
	private UserRepo userRepo;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> optionalUsers = userRepo.findByName(username);
		
		optionalUsers.
			orElseThrow(()-> new UsernameNotFoundException("username not found"));
		
		return optionalUsers.map(CustomUserDetails::new).get();
	
	}
	
	
}
