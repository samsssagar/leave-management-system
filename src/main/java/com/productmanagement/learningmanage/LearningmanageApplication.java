package com.productmanagement.learningmanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearningmanageApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearningmanageApplication.class, args);
	}

}
