package com.productmanagement.learningmanage.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.productmanagement.learningmanage.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Integer>{

	Optional<User> findByName(String username);
	
}
